# Trisquel GNU/Linux

## License
GNU General Public License https://www.gnu.org/licenses/gpl-3.0.txt

## Project status
Desktop for End Users, stable in use<br>

## Download
Downloading Trisquel ISOs are available at https://linux.sakura.ne.jp/trisquel-iso/ <br>
As for small size Netinst ISO, it is also available https://gitlab.com/green59/trisquel/-/tree/main/trisquel-netinst

See for more details at the Official website of https://trisquel.info/en